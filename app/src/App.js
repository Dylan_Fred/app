import React,{ Component } from 'react';
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import Product from './components/Product';
import ProductList from './components/ProductList';
import Details from './components/Details';
import Cart from './components/Cart';
import Modal from './components/Modal.js';



function App() {
  return (
    <React.Fragment>
     <Navbar />
     <Switch>
        <Route  exact path="/" component={ProductList}/> 
        <Route  path="/details" component={Details}/>
        <Route  path="/cart" component={Cart}/>
        
     </Switch>
     <Modal />
     
     
    </React.Fragment>
  );
}

export default App;
